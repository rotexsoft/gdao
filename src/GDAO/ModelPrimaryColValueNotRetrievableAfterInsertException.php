<?php
declare(strict_types=1);

namespace GDAO;

class ModelPrimaryColValueNotRetrievableAfterInsertException extends \Exception {}
